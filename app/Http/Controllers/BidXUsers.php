<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BidXUser;
use App\User;

class BidXUsers extends Controller
{
    
    /**
     * create bid
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $userId
     * @param int $productId
     * @param float $bidAmount
     */
    public function createBid(Request $request){
        $bidXUser = New BidXUser();
        $result = $bidXUser->createBidByUser($request->userId, $request->productId, $request->bidAmount);
        
        return array("result" => $result);
    }

    /**
     * get Max bid by product
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $productId
     */
    public function getMaxBid(Request $request){
        $bidXUser = New BidXUser();
        
        return array('max_bid' => $bidXUser->getMaxBid($request->productId));
    }

    /**
     * enable auto bidding
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $userId
     * @param int $productId
     * @param bool $enableDisableAB
     */
    public function enableAutoBidding(Request $request){
        $bidXUser = New BidXUser();
        $result =  $bidXUser->enableAutobiding($request->userId, $request->productId, $request->enableDisableAB);
        
        return array("result" => $result);
    }

    /**
     * execute auto bidding
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $id
     */
    public function executeAutobidding(Request $request){
        $bidXUser = New BidXUser();
        $result = $bidXUser->runAutoBidding($request->productId);
        
        return array("result" => $result);
    }
}
